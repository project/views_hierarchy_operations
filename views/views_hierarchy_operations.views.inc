<?php
// $Id:
/**
 * @file apci_group_navigator.views.inc
 * Built in plugins for Views output handling.
 *
 */

/**
 * Implementation of hook_views_plugins
 */
function views_hierarchy_operations_views_plugins() {
  $path = drupal_get_path('module', 'views_hierarchy_operations');
  $plugins = array(
    'module' => 'views',
    'style' => array(
      'views_hierarchy_operations' => array(
        'title' => t('Hierarchy Operations'),
        'help' => t('Display\'s a Views Bulk Operations within a hierarchy tree format.'),
        'handler' => 'views_hierarchy_operations_plugin_style',
        'parent' => 'bulk',
        'path' => "$path/views",
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'theme' => 'views_view_bulk', 
        'even empty' => TRUE,
      )
    )
  );
  return $plugins;
}