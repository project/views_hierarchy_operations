(function($) {
  $(function() {
    // Setup the hide/show functionality of the hierarchy.
    $(".views_hierarchy_op_fieldset").hide();
    $("a.views_hierarchy_op_expand_link").each(function() {
      if(!$(".views_hierarchy_op_fieldset_" + $(this).attr('href')).length) {
        $("div.views_hierarchy_op_expand", this).hide();
      }
      else {
        $(this).click( function( event ) {
          event.preventDefault();
          var fieldset = $(".views_hierarchy_op_fieldset_" + $(this).attr('value'));
          if( fieldset.is(":visible") ) {
            fieldset.hide("fast");
            $("div.views_hierarchy_op_expand", this).addClass('collapsed');
          }
          else {
            fieldset.show("fast");
            $("div.views_hierarchy_op_expand", this).removeClass('collapsed');
          }
        });
      }
    });

    // Setup the select all functionality.
    var strings = { 'selectAll': Drupal.t('Select all rows in this table'), 'selectNone': Drupal.t('Deselect all rows in this table') };
    $(".views_hierarchy_operations").each(function() {
      var _this = this;
      $('th.vho-select-all', this).prepend($('<input type="checkbox" class="form-checkbox vho-select-all-checkbox" />').attr('title', strings.selectAll)).click(function(event) {
        if ($(event.target).is('input:checkbox')) {
          var checked = $(event.target).is(":checked");
          $("input", _this).each(function() {
            if( !$(this).hasClass('vho-select-all-checkbox') ) {
              $(this).attr('checked', checked);
            }
          });
        }
      });
    });

    Drupal.views_hierarchy_operations = {
      onClick: function( checkfield ) {
        // Make the child checkboxes reflect this parent checkbox.
        $("input." + $(checkfield).attr('parentselect')).attr('checked', $(checkfield).is(":checked"));
      }
    };
  });
})(jQuery);


